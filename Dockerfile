ARG IMAGE_NAME=debian
ARG ENV

# DEBIAN BASED
FROM debian:10-slim AS debian
RUN apt update \
    && apt install -qqy ca-certificates openssh-client bash curl \
    && apt autoremove --purge -qqy \
    && apt autoclean \
    && rm -rf /var/lib/apt/lists/*

# ALPINE BASED
FROM alpine:3.5 AS alpine
RUN apk add --no-cache ca-certificates openssh-client bash curl

# FINAL
FROM ${IMAGE_NAME} AS final

ARG RANCHER_CLI_VERSION=0.6.7
ENV RANCHER_CLI_VERSION=${RANCHER_CLI_VERSION}

RUN cd /tmp \
    && curl https://releases.rancher.com/cli/v${RANCHER_CLI_VERSION}/rancher-linux-amd64-v${RANCHER_CLI_VERSION}.tar.gz > rancher.tar.gz \
    && tar xvzf rancher.tar.gz \
    && cp ./rancher-v${RANCHER_CLI_VERSION}/rancher /usr/bin/ \
    && rm -rf /tmp/rancher*

SHELL [ "/bin/bash" ]

ENTRYPOINT [ "rancher" ]

CMD  [ "--help" ]
